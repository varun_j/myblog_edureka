from django import forms
from models import User

# class SignUpForm(forms.Form):
# 	first_name = forms.CharField(strip=True)
# 	last_name = forms.CharField(strip=True)
# 	cities = forms.ChoiceField(choices=[('delhi','Delhi'), ('new_york','New York')])
# 	gender = forms.ChoiceField(choices=[('m','Male'), ('f','Female')], widget=forms.RadioSelect())



class SelectionForm(forms.Form):
	choices = forms.ChoiceField(choices=[('1', 'Choice One'), ('2', 'Choice Two')], widget=forms.RadioSelect())


#https://docs.djangoproject.com/en/1.11/topics/forms/modelforms/

class SignUpForm(forms.ModelForm):

	class Meta:
		model = User
		fields = ['first_name', 'last_name']

