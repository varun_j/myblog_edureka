# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# CLOB AND BLOB
# QUERY MANAGER
# Composite primary key


# content_user

class User(models.Model):

	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	gender = models.CharField(max_length=1,choices=[('m','Male'), ('f','Female')])
	cities = models.CharField(max_length=255)
	has_been_activated = models.BooleanField(default=True)
	# runs once
	created_on = models.DateTimeField(auto_now_add=True)
	# runs every time the record is updated
	updated_on = models.DateTimeField(auto_now=True)


	def __str__(self):
		return self.first_name

#content_post

class Post(models.Model):

	post_title = models.CharField(max_length=255)
	post_text = models.CharField(max_length=4000)
	author = models.ForeignKey('User')

# author_id





# Models.py
'''
1. No more DDL queries. All dbs are synced up by just pulling the latest code
2. No more DML queries. Way easier to deal with python code.
'''
#Object Relational Mapper






# Many to many
# https://docs.djangoproject.com/en/1.11/ref/models/fields/#django.db.models.ManyToManyField.through_fields