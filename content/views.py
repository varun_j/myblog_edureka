

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpRequest

from forms import SignUpForm, SelectionForm

from models import User
# Create your views here.


def home_view(request):

	if request.method == "POST":
		form_data = SelectionForm(request.POST)

		if form_data.is_valid():
			selection =  int(form_data.cleaned_data.get('choices', None))
			signup = SignUpForm()

			if selection == 1:
				return render(request, 'user-type-1.html', {'signup': signup})
			else:
				return render(request, 'user-type-2.html',{'signup': signup})
		else:
			print 'Form is invalid'
			return render(request, 'home.html', {'signup_form': form_data})
	else:
		signup_form = SelectionForm()

		return render(request, 'home.html', {'signup_form': signup_form})


#Using forms.Form
def signup_one(request):

	data = SignUpForm(request.POST)

	if data.is_valid():
		print 'On line 41'
		new_user = User(first_name = data.cleaned_data.get('first_name'),
						last_name = data.cleaned_data.get('last_name'),
						gender = data.cleaned_data.get('gender'),
						cities = data.cleaned_data.get('cities')
						)
		new_user.save()


# Using forms.ModelForm
def signup_one(request):

	data = SignUpForm(request.POST)

	if data.is_valid():
		print 'On line 41'
		x = data.save()
		print x.id


def post_view(request):

	return render(request, 'post.html')


