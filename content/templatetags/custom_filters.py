import re

from django import template


register = template.Library()


@register.filter
def title_parser(value,arg=None):
	#python_django
	if arg == 'html':
		name = " ".join(value.split("_"))
		return "<h3> %s </h3>" % (name)
	else:
		return " ".join(value.split("_"))



# {{ value | result_parser:'marks_cloumn' }}
@register.filter
def result_parser(value, arg=None):
	if arg == 'marks_column':
		result_mapper = {
			"pass": "",
			"fail": " *",
			"due": " *",
			"grace": " G",
			"absent": "AA",
			"todo-s": " *"
		}
	else:
		result_mapper = {
			"pass": "P",
			"fail": "D",
			"due": "D",
			"grace": "G",
			"absent": "D",
			"todo-s": "D"
		}

	return result_mapper[value]


@register.filter
def parse_composite_result(value):
	return {
		"pass": "PASS",
		"fail": "DUE",
		# Specific to IIS
		"pieo": "PIEO",
		"pifo": "PIFO"
	}[value]






















#
# form = PostForm(request.POST, request.FILES)
# if form.is_valid():
# 	image = form.cleaned_data.get('image')
# 	caption = form.cleaned_data.get('caption')
# 	post = PostModel(user=user, image=image, caption=caption)
# 	post.save()
#
# 	path = str(BASE_DIR + post.image.url)
#
# 	client = ImgurClient(YOUR_CLIENT_ID, YOUR_CLIENT_SECRET)
# 	post.image_url = client.upload_from_path(path, anon=True)['link']
# 	post.save()